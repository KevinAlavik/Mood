# Mood - Modular Game Framework
The easiest way for creating games in C

# Exclaimer!
Creating mods isnt really that easy, you have to have SDL2 knowledge and also alot of C knowledge. But using these mods to further create games is easier!

## Description
Mood is a modular game framework written in C designed for Linux/Unix systems. It provides a set of pre-built modules for common functionalities such as GUI, logging, textures handling, window control, scenes management, and user input handling. Mods are compiled on startup, making it highly customizable and suitable for various game development projects.

## Features
- Modular design for easy extensibility and customization.
- Pre-built modules for GUI, logging, textures handling, window control, scenes management, and user input handling.
- Supports compilation of mods on startup.
- Suitable for game development projects on Linux/Unix systems.
- A way to easly create games.

## Installation
1. Clone the Mood repository from GitLab: [repository link](https://gitlab.com/KevinAlavik/Mood).
2. Refer to the wiki for detailed documentation and usage instructions: [wiki link](https://gitlab.com/KevinAlavik/Mood/-/wikis/home).
3. Follow the installation instructions below.

## Installation Instructions
- Navigate to the root directory of the Mood framework.
- Open a terminal and run the following commands:
  ```bash
  make install
  ```
- This will symlink the executable to `/usr/local/bin` making it accessible system-wide.

### Config
You can also change the makefiles TARGET_FPS, deafult is 120 (set to -1 for unlimited). This changes the max fps. This can be accessed thru mods (more on that in the [wiki](https://gitlab.com/KevinAlavik/Mood/-/wikis/home)).

### Other methods
There are also some other things in the makefile:
- `make`
    - This just builds the Mood exec into the **bin** directory
- `make debug`
    - This is kinda like just `make` just that it adds support for the DEBUG_LOG.
- `make clean`
    - This just cleans the **bin** directory



## Usage
1. Follow the installation instructions to install Mood.
2. Explore the provided sample code and examples to understand how to utilize Mood for your game development projects.

For more information, visit the Mood GitLab repository and wiki.
- Repository: [repository link](https://gitlab.com/KevinAlavik/Mood)
- Wiki: [wiki link](https://gitlab.com/KevinAlavik/Mood/-/wikis/home)

## Note
Ensure compliance with the GPLv3 license terms when using or distributing Mood framework or any modifications/extensions made to it.

## License
Mood is licensed under the GNU General Public License version 3 (GPLv3).
