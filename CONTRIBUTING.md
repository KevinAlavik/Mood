# Contributing to Mood

Thank you for considering contributing to Mood! We appreciate your interest in improving the project.

## How to Contribute

### Reporting Bugs

If you encounter a bug while using Mood, please ensure it has not been reported already by checking the issues section. If it hasn't been reported yet, please open a new issue and include as much detail as possible, such as:

- Description of the bug
- Steps to reproduce
- Expected behavior
- Any relevant screenshots or code snippets

### Submitting Enhancements

We welcome enhancements and new features to improve Mood. Before submitting a pull request, please discuss the change you wish to make via issue, email, or any other method with the maintainers of this repository.

### Pull Request Process

1. Fork the repository and create a new branch for your contribution.
2. Ensure your code follows the coding style and conventions of the project.
3. Include relevant tests and ensure all existing tests pass.
4. Update the documentation to reflect your changes, if applicable.
5. Add a summary of your changes to the `Changelog` file following the existing format.
6. Ensure your commit messages are clear and descriptive.
7. Submit a pull request, providing a detailed description of the changes introduced.

## Code Style

When contributing code to Mood, please adhere to the following coding style guidelines:

- Use 4-space tabs for indentation.
- Write function names and variables in snake_case (Write important and/or internal functions and variables in pascal case and for more then two words do pascal_snake_case), like `function_name_like_this()`, `int INTERNAL_VARIABLE_LIKE_THIS = 10`.
- Define structs using `typedef struct {} structname_t;`.
- Write concise and well-commented code to improve readability.

## License

By contributing to Mood, you agree that your contributions will be licensed under the [GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.html)

![GNU General Public Liecense v3.0](https://www.gnu.org/graphics/gplv3-127x51.png)
 
 . For more information, please refer to the `LICENSE`
