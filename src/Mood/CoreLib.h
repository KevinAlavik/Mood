#ifndef __CORELIB_H__
#define __CORELIB_H__

#include <Mood/SGUI.h>
#include <Mood/Window.h>
#include <Mood/Includes.h>

#define WINDOWPOS_CENTERED SDL_WINDOWPOS_CENTERED
#define TARGET_FPS 120

#endif // __CORELIB_H__
