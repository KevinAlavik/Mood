#ifndef __WINDOW_H__
#define __WINDOW_H__

#include <Mood/Internal/CLogger.h> 
#include <Mood/Includes.h>

#define MAX_WINDOWS 10

typedef struct {
    int width;
    int height;
    int x;
    int y;
    char* title;
} MOOD_WindowConfig;

typedef struct {
    MOOD_WindowConfig* config_ptr;
    SDL_Window* window_ptr;
    SDL_Renderer* renderer_ptr;
    int centerX;
    int centerY;
    bool alive;
    uint8_t id;
} MOOD_Window;

MOOD_Window MOOD_SpawnWindow(MOOD_WindowConfig* config);
int MOOD_DestroyWindow(MOOD_Window* window);
void MOOD_HandleWindowEvents(MOOD_Window* window);
MOOD_Window* MOOD_FetchWindow(uint8_t id);

#endif // __WINDOW_H__