#ifndef __INCLUDES_H__
#define __INCLUDES_H__

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <time.h>
#include <string.h>
#include <dirent.h>
#include <dlfcn.h>
#include <limits.h>
#include <unistd.h> 

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>


#endif // __INCLUDES_H__
