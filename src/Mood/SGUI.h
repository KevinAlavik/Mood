#ifndef __SGUI_H__
#define __SGUI_H__

#include <Mood/Window.h>

void MOOD_ChangeWindowBG(MOOD_Window* window, Uint8 r, Uint8 g, Uint8 b);

#endif // __SGUI_H__
