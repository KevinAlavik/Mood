#include "Window.h"

#include <stdlib.h>

static MOOD_Window windows[MAX_WINDOWS];
static int numWindows = 0;

MOOD_Window MOOD_SpawnWindow(MOOD_WindowConfig* config) {
    srand((unsigned int)time(NULL));
    
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        MOOD_ConsoleLog(stderr, "Error", "SDL initialization failed: %s", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    if (IMG_Init(IMG_INIT_PNG) < 0) {
        MOOD_ConsoleLog(stderr, "Error", "SDL Image initialization failed: %s", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    DEBUG_LOG("SDL initialized successfully");

    if (numWindows >= MAX_WINDOWS) {
        MOOD_ConsoleLog(stderr, "Error", "Maximum number of windows reached");
        exit(EXIT_FAILURE);
    }

    MOOD_Window window;

    window.window_ptr = SDL_CreateWindow(
        config->title,
        config->x,
        config->y,
        config->width,
        config->height,
        SDL_WINDOW_SHOWN
    );
    if (window.window_ptr == NULL) {
        MOOD_ConsoleLog(stderr, "Error", "SDL window creation failed: %s", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    DEBUG_LOG("SDL window created successfully");

    window.renderer_ptr = SDL_CreateRenderer(window.window_ptr, -1, SDL_RENDERER_ACCELERATED);
    if (window.renderer_ptr == NULL) {
        MOOD_ConsoleLog(stderr, "Error", "SDL renderer creation failed: %s", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    DEBUG_LOG("SDL renderer created successfully");

    window.config_ptr = config;
    window.centerX = config->width / 2;
    window.centerY = config->height / 2;
    window.alive = true;
    window.id = numWindows;
    windows[numWindows++] = window;

    DEBUG_LOG("Window spawned successfully");
    DEBUG_LOG(" - Assigned id %d to window.", window.id);

    return window;
}

int MOOD_DestroyWindow(MOOD_Window* window) {
    if (window == NULL)
        return -1;

    if (window->renderer_ptr)
        SDL_DestroyRenderer(window->renderer_ptr);

    if (window->window_ptr)
        SDL_DestroyWindow(window->window_ptr);

    IMG_Quit();
    SDL_Quit();

    return 0;
}

void MOOD_HandleExitSignal(int signal) {
    if (signal == SIGINT) {
        MOOD_ConsoleLog(stdout, "Mood Exit", "Recived a SIGINT signal, goodbye.");
        exit(EXIT_SUCCESS);
    }
}

void MOOD_HandleWindowEvents(MOOD_Window* window) {
    signal(SIGINT, MOOD_HandleExitSignal);

    SDL_Event event;
    int startTime = SDL_GetTicks();

    while (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT:
                window->alive = 0;
                break;
            default:
                break;
        }
    }

    int elapsedTime = SDL_GetTicks() - startTime;
    int frameDelay = 1000 / TARGET_FPS;

    if (elapsedTime < frameDelay) {
        SDL_Delay(frameDelay - elapsedTime);
    }
}

MOOD_Window* MOOOD_GetWindow(uint8_t id) {
    if (id >= numWindows)
        return NULL;

    return &windows[id];
}
