#include "SGUI.h"

void MOOD_ChangeWindowBG(MOOD_Window* window, Uint8 r, Uint8 g, Uint8 b) {
    SDL_Renderer* renderer = SDL_GetRenderer(window->window_ptr);
    SDL_SetRenderDrawColor(renderer, r, g, b, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);

    DEBUG_LOG("Set window %d's background color.", window->id);
}