#ifndef __CLOGGER_H__
#define __CLOGGER_H__

#include <Mood/Includes.h>

#ifdef DEBUG_LOGGING
#define DEBUG_LOG(...) MOOD_ConsoleLog(stdout, "Mood Debug", __VA_ARGS__)
#else
#define DEBUG_LOG(...) ((void)0)
#endif

void MOOD_ConsoleLog(FILE* stream, const char* tag, const char* format, ...);

#endif // __CLOGGER_H__
