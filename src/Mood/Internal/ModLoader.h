#ifndef __MODLOADER_H__
#define __MODLOADER_H__

#include <Mood/Includes.h>

int MOOD_CompileMods(const char *dir_path);
int MOOD_LoadMod(const char *path);

#endif // __MODLOADER_H__
