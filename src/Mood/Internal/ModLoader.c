#include "ModLoader.h"
#include <Mood/Internal/CLogger.h>

void remove_so_files_recursive(const char *dir_path) {
    DIR *dir;
    struct dirent *entry;

    dir = opendir(dir_path);
    if (!dir) {
        MOOD_ConsoleLog(stderr, "Mood Error", "Error opening directory %s", dir_path);
        return;
    }

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_DIR) {
            if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
                char sub_dir[PATH_MAX];
                snprintf(sub_dir, sizeof(sub_dir), "%s/%s", dir_path, entry->d_name);
                remove_so_files_recursive(sub_dir);
            }
        } else if (entry->d_type == DT_REG) {
            if (strstr(entry->d_name, ".so") != NULL) {
                char so_path[PATH_MAX];
                snprintf(so_path, sizeof(so_path), "%s/%s", dir_path, entry->d_name);
                if (unlink(so_path) != 0) {
                    MOOD_ConsoleLog(stderr, "Mood Error", "Error removing .so file: %s", so_path);
                }
            }
        }
    }

    closedir(dir);
}

int compile_mod(const char *mod_dir) {
    remove_so_files_recursive(mod_dir);

    DIR *dir;
    struct dirent *entry;
    char command[PATH_MAX + PATH_MAX + 200];

    dir = opendir(mod_dir);
    if (!dir) {
        MOOD_ConsoleLog(stderr, "Mood Error", "Error opening directory %s", mod_dir);
        return 1;
    }

    DEBUG_LOG("Compiling mods in directory: %s", mod_dir);

    char *home_path = getenv("HOME");
    if (!home_path) {
        MOOD_ConsoleLog(stderr, "Mood Error", "Failed to get HOME environment variable");
        closedir(dir);
        return 1;
    }

    char mood_path[PATH_MAX];
    snprintf(mood_path, sizeof(mood_path), "%s/.mood/info/path", home_path);

    FILE *fp = fopen(mood_path, "r");
    if (!fp) {
        MOOD_ConsoleLog(stderr, "Mood Error", "Error opening %s", mood_path);
        closedir(dir);
        return 1;
    }
    
    char mood_dir[PATH_MAX];  // New variable to store mood directory path
    if (fgets(mood_dir, sizeof(mood_dir), fp) == NULL) {  // Read mood directory path
        fclose(fp);
        MOOD_ConsoleLog(stderr, "Mood Error", "Error reading ~/.mood/info/path");
        closedir(dir);
        return 1;
    }
    fclose(fp);
    mood_dir[strcspn(mood_dir, "\n")] = 0;

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            char mod_path[PATH_MAX];
            snprintf(mod_path, sizeof(mod_path), "%s/%s", mod_dir, entry->d_name);
            snprintf(command, sizeof(command), "gcc -shared -I%s/src -o %s.so %s %s/bin/Mood/*.o", mood_dir, mod_path, mod_path, mood_dir);  // Use mood_dir instead of mood_path
            DEBUG_LOG("Compiling mod: %s", entry->d_name);
            if (system(command) != 0) {
                MOOD_ConsoleLog(stderr, "Mood Error", "Error compiling mod: %s", entry->d_name);
                closedir(dir);
                return 1;
            }
        }
    }

    closedir(dir);

    return 0;
}

int MOOD_CompileMods(const char *dir_path) {
    DIR *dir;
    struct dirent *entry;
    char absolute_path[PATH_MAX];
    int compile_count = 0;

    dir = opendir(dir_path);
    if (!dir) {
        MOOD_ConsoleLog(stderr, "Mood Error", "Error opening directory %s", dir_path);
        return -1;
    }

    DEBUG_LOG("Compiling mods in directory: %s", dir_path);

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_DIR && strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            snprintf(absolute_path, sizeof(absolute_path), "%s/%s", dir_path, entry->d_name);
            DEBUG_LOG("Found mod! (%s)", absolute_path);
            if (compile_mod(absolute_path) == 0) {
                DEBUG_LOG("Successfully compiled %s.", absolute_path);
                compile_count++;
            } else {
                MOOD_ConsoleLog(stderr, "Mood Error", "Failed to compile %s!", absolute_path);
                exit(1);
            }
        }
    }

    closedir(dir);

    return compile_count;
}

int MOOD_LoadMod(const char *path) {
    char absolute_path[PATH_MAX];
    if (realpath(path, absolute_path) == NULL) {
        MOOD_ConsoleLog(stderr, "Mood Error", "Error resolving absolute path for %s", path);
        return 1;
    }

    DEBUG_LOG("Loading mod: %s", absolute_path);

    void *mod_handle = dlopen(absolute_path, RTLD_LAZY);
    if (!mod_handle) {
        MOOD_ConsoleLog(stderr, "Mood Error", "Error loading mod file: %s", dlerror());
        return 1;
    }

    void (*mod_entry)(void) = dlsym(mod_handle, "mod_entry");
    if (!mod_entry) {
        MOOD_ConsoleLog(stderr, "Mood Error", "Error locating mod_entry function: %s", dlerror());
        dlclose(mod_handle);
        return 1;
    }

    DEBUG_LOG("Successfully loaded mod: %s", absolute_path);

    mod_entry();
    dlclose(mod_handle);

    return 0;
}
