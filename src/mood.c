#include <Mood/CoreLib.h>
#include <Mood/Internal/CLogger.h>
#include <Mood/Internal/ModLoader.h>

void run_mod_files(const char *dir_path) {
    DIR *dir;
    struct dirent *entry;

    dir = opendir(dir_path);
    if (!dir) {
        MOOD_ConsoleLog(stderr, "Mood Error", "Error: Unable to open directory %s", dir_path);
        return;
    }

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG && strstr(entry->d_name, ".so") != NULL) {
            char mod_path[PATH_MAX];
            snprintf(mod_path, sizeof(mod_path), "%s/%s", dir_path, entry->d_name);
            MOOD_ConsoleLog(stdout, "Mood Info", "Loading mod %s", mod_path);
            int load_result = MOOD_LoadMod(mod_path);
            if (load_result != 0) {
                MOOD_ConsoleLog(stderr, "Mood Error", "Error: Failed to load mod %s", mod_path);
            }
        } else if (entry->d_type == DT_DIR && strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            char subdir_path[PATH_MAX];
            snprintf(subdir_path, sizeof(subdir_path), "%s/%s", dir_path, entry->d_name);
            run_mod_files(subdir_path); // Recursively run for subdirectories
        }
    }

    closedir(dir);
}

int main(int argc, char *argv[]) {
    char *mod_dir = getenv("HOME"); // Get the home directory
    if (!mod_dir) {
        MOOD_ConsoleLog(stderr, "Mood Error", "Error: Unable to get home directory");
        return 1;
    }

    // Append the .mood/mods directory to the home directory
    char absolute_path[PATH_MAX];
    snprintf(absolute_path, sizeof(absolute_path), "%s/.mood/mods", mod_dir);

    int compile_result = MOOD_CompileMods(absolute_path);
    if (compile_result < 0) {
        MOOD_ConsoleLog(stderr, "Mood Error", "Error: Failed to compile mods\n");
        return 1;
    } else if (compile_result == 0) {
        DEBUG_LOG("Found no mods?");
    } else {
        DEBUG_LOG("Successfully compiled %d mods\n", compile_result);
    }

    run_mod_files(absolute_path);

    return 0;
}